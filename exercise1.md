- ## Función de las dependencias instaladas.

  - **Express**: Nos permite escribir manejadores de peticiones con diferentes verbos HTTP (GET, POST, PUT, PATCH, DELETE) en diferentes caminos URL(rutas)

  - **Dotenv** : configurar las diferentes variables de entorno a traves del archivo .env
  - **@types/express**: contiene el mismo código que Express, pero adecuado para utilizarlo con TypeScript.
  - **@types/node**: Contiene el código necesari para que Nodejs puede funcionar con TypeScript.
  - **Nodemon**: nos srive de herramienta para correr el servidor.
  - **@types/jest**: ayudar a tener una mayor compatibilidad al escribir tus pruebas con TypeScript.
  - **Eslint**: es un linter para JS, que nos ayuda a cumplir las buenas prácticas decodificación y de estilos en un lenguaje de programación.
  - **Jest**: libreria que nos permite escribir y ejecutar tests.
  - **webpack**: sirve para separar el código en módulos que luego se utilizan como dependencias en otros módulos de nuestro proyeto.

- ## Scripts de NPM creados.
  Ejecutan lo que esta dentro de comillas " "dentro de nuestro package.json
  - **"build"**: genera el index.js a traves del tsconfig.json
  - **"start"**: ejecutar el código transpilado del archivo index.js en la carpeta dist, a nivel de raiz
  - **"dev"**: para realizar pruebas.
  - **"test"**: ejecutar los test a traves de jest.
  - **"serve:coverage"**: ejecuta el test, luego el coverage que genera la carpeta lcov-report, para los informes de coverage
