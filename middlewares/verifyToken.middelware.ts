import jwt from 'jsonwebtoken'; 
import { Request, Response, NextFunction} from 'express'; 


import dotenv from 'dotenv'; 

// Config dotend to read enviromenr variables
dotenv.config(); 

const secret = process.env.SECRETKEY || 'MYSCREKEY'; 
/**
 * 
 * @param  { Request } req original request previous middleware of verification JWT
 * @param  { Response } res Response to verification of JWt
 * @param  { Next Function } next Next function to be executed
 * @returns  Error of verification or next execution 
 */


export const verifyToken = ( req:Request, res:Response, next:NextFunction) => {

  // CHeck HEADER from Request for 'x-access-token'
  let token: any = req.headers['x-access-token']; 

  // Verify if jwt is present
  if(!token){
    return res.status(403).send({
      authenticationError: 'Missing JWt in request', 
      message: 'Not authorised to consume this endpoint'
    }); 
  }

  // Verify the token obtained
  jwt.verify(token, secret, (err:any, decoded:any) => {
        if(err){
          return res.status(500).send({
            authenticationError: 'JWt verification failed', 
            message: 'Failed to verify JWT token in request'
          }); 
        }

        //Pass something to next request (id of user) || other 

        // Execute Next Function -> Protected Routes will be executed
        next()
  })


}