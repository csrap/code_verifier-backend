import { DateResponse} from "./types";
import { IhelloController} from "./interfaces";
import { LogSuccess } from "../utils/Logger"; 

const timeElapsed = Date.now();
const today = new Date(timeElapsed);

export class GoodByeCtrl implements IhelloController {
  public async getMessage(name?: string): Promise<DateResponse> {
    LogSuccess('[api/goodbye] Get Request'); 

    return {
      message: `Goodbye ${name || "world!"}`,
      Date: `${today.toUTCString()}`, 
    }
  }

}