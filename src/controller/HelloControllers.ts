import { Get, Query, Route, Tags} from "tsoa";
import { BasicResponse} from "./types";
import { IhelloController} from "./interfaces";
import { LogSuccess } from "../utils/Logger"; 

@Route("/api/hello")
@Tags("HelloController")
export class HelloController implements IhelloController {
    /**
     * Endpoint to retreive a Message "Hello(name) in JSON"
     * @param name Name of user be greeted
     * @return  { BasicResponse } Promise of BasicResponse
     */
    @Get("/")
  public async getMessage(@Query()name?: string): Promise<BasicResponse> {
    LogSuccess('[api/hello] Get Request'); 

    return {
      message: `Hello ${name || "world!"}`
    }
  }

}