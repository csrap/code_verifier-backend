import { Get, Query, Route, Tags, Delete, Post, Put} from "tsoa";
import { IKataController} from "./interfaces";
import { LogSuccess, LogError, LogWarning} from "../utils/Logger"; 

//ORM - Users Collection

import {  getAllKatas, getKataByID, deleteKatasByID, createKata, updateKataByID} from "../domain/orm/Kata.orm"; 
import { IKata } from "../domain/interfaces/IKata.interface";


@Route("api/kata")
@Tags("KatasController")

export class KatasController implements IKataController{
  /**
   * EndPoint to retreive the katas in the Collection "Katas " of DB
   * @param {string} id Id of kata to retretive (optional)
   * @return All katas o kata founf by ID
   */

  @Get("/")
  public async  getKatas(@Query()page: number, limit: number, id?: string): Promise<any> {
    let response : any = ''; 

    if(id){
      LogSuccess(`[/api/katas] Get Kata By ID: ${id} `);
      response = await getKataByID(id);
      
    }else {
      LogSuccess('[/api/katas] Get All Katas Request')
      response = await getAllKatas(page, limit);
    }
    
    return response;
  }
  
  //*Obtain from tsoa
  /**
   * EndPoint to delete  the Katas in the Collection "Katas" of DB
   * @param {string} id Id of kata to delete (optional)
   * @return message informain if delection was correct
   */
  @Delete("/")
  public async deleteKata(@Query()id?: string): Promise<any> {
    
    let response: any = ''; 
    
    if(id){
      LogSuccess(`[/api/katas] Delete Kata by ID: ${id}`); 
      await deleteKatasByID(id).then((r) => {
        response = {
          message: `Kata with id ${id} delete successfully`
        }
      }) 
    }else {
      LogWarning(`[/api/katas] Delete User Request WITHOUT ID`)
      response = {
        message: 'Please, provide an ID to remove from database'
      }
    }
      return response; 
  }
  
  @Post("/")
  public async createKata(kata: IKata): Promise<any> {
    
    let response: any = ''; 
    
    if(kata){
      LogSuccess(`[/api/katas] Create New Kata: ${kata.name} `);
      await createKata(kata).then((r) => {
          LogSuccess(`[/api/katas] Created Kata: ${kata.name} `);
          response = {
              message: `Kata created successfully: ${kata.name}`
          }
      });
  }else {
      LogWarning('[/api/katas] Register needs Kata Entity')
      response = {
          message: 'Kata not Registered: Please, provide a Kata Entity to create one'
      }
  }
  return response;
}


  @Put("/")
  public async updateKata(@Query()id: string, kata: IKata): Promise<any> {
    
      let response : any = ''; 

      if(id){
        LogSuccess(`[/api/katas] Update Kata by ID: ${id}`); 
        await updateKataByID(id, kata).then((r) => {
          response = {
                  message: `Kata with id ${id} updated successfully`
          }
        })
      }else {
        LogWarning(`[/api/katas] Update Kata Request WITHOUT ID`)
        response = {
          message: 'Please, provide an ID to update an existing kata'
        }
      }

        return response; 
    }
    
}
