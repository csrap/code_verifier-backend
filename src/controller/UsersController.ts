import { Get, Query, Route, Tags, Delete, Post, Put} from "tsoa";
import { IUserController} from "./interfaces";
import { LogSuccess, LogError, LogWarning} from "../utils/Logger"; 

//ORM - Users Collection

import {  getAllUsers, getUserByID, deleteUserByID,  updateUserByID, getKatasFromUser} from "../domain/orm/User.orm"; 
import { getAllKatas, getKataByID } from "../domain/orm/Kata.orm";



@Route("api/users")
@Tags("UserController")

export class UserController implements IUserController {
  /**
   * EndPoint to retreive the Users in the Collection "Users " of DB
   * @param {string} id Id of user to retretive (optional)
   * @return All user o user founf by ID
   */
 
  public async getUsers(@Query()page: number, @Query()limit: number, @Query()id?: string): Promise<any> {
    
    let response: any = '';
    
    if(id){
      LogSuccess(`[/api/users] Get User By ID: ${id} `);
      response = await getUserByID(id);
      
    }else {
      LogSuccess('[/api/users] Get All Users Request')
      response = await getAllUsers(page, limit);
    }
    
    return response;
  }
  
  
  //*Obtain from tsoa
  /**
   * EndPoint to delete  the Users in the Collection "Users " of DB
   * @param {string} id Id of user to delete (optional)
   * @return message informain if delection waaas correct
   */
  @Delete("/")
  public async deleteUser(@Query()id?: string): Promise<any> {
    
    let response: any = ''; 
    
    if(id){
      LogSuccess(`[/api/users] Delete User by ID: ${id}`); 
      await deleteUserByID(id).then((r) => {
        response = {
          status: 204,
          message: `User with id ${id} delete successfully`
        }
      }) 
    }else {
      LogWarning(`[/api/users] Delete User Request WITHOUT ID`)
      response = {
        status: 400, 
        message: 'Please, provide an ID to remove from database'
      }
    }
    return response; 
  }
  
  
  
  @Put("/")
  public async updateUser(@Query()id: string, user: any): Promise<any> {
    
    let response : any = ''; 
    
    if(id){
      LogSuccess(`[/api/users] Update User by ID: ${id}`); 
      await updateUserByID(id, user).then((r) => {
        response = {
          message: `User with id ${id} updated successfully`
        }
      })
    }else {
      LogWarning(`[/api/users] Update User Request WITHOUT ID`)
      response = {
        message: 'Please, provide an ID to update an existing user'
      }
    }
    
    return response; 
  }
  @Get("/katas") //users/kata
  public async  getKatas(@Query()page: number, @Query()limit: number, @Query()id?: string): Promise<any> {
    let response : any = ''; 

    if(id){
      LogSuccess(`[/api/users/katas] Get Katas from User By ID: ${id} `);
      response = await getKatasFromUser(page, limit, id);
      
    }else {
      LogSuccess('[/api/users/katas] Get All Katas without id')
      response = {
        message: 'ID user is needed'
      }
    }
    
    return response;
  }
}


