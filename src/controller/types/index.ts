

/**
 * Basis JSon response for Controllers
 */

export type BasicResponse = {
  //JSON de respuesta 
  message: string,
}

/**
 * Error JSON response for Controllers
 */

export type ErrorResponse = {
  error: string,
  message: string
}

export type DateResponse = {
  //JSON de respuesta 
  message: string,
  Date: string, 
}

/**
 * Auth JSON response for Controllers
 */
export type AuthResponse = {
  message: string,
  token: string
}
