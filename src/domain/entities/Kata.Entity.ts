import mongoose from "mongoose";
import { IKata } from "../interfaces/IKata.interface"


export const kataEntity = () => {

  let kataSchema = new mongoose.Schema (
    {
      name: { type: String, required: true },
      description: { type: String, required: true },
      level: { type: String, required: true },  
      chances: { type:Number, required: true },  
      stars: { type:Number, required: true },  
      user: { type: String, required: true }, 
      solution:{ type: String, required: true },
      participants: { type: [], required: true},
    }
  )

  return mongoose.models.Katas || mongoose.model<IKata>('Katas', kataSchema);
}