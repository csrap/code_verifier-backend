export enum KataLevel {
  BASIC = 'Basic',
  MEDIUM = 'Medium',
  HIGH = 'High'
}

export interface IKata {
  name: string,
  description: string,
  level: KataLevel,
  chances: number,
  stars: number,
  user: string, // Id of user
  solution: string,
  participants: string[]
}
