import { IKata } from "./IKata.interface";

export interface IUser {
  name: String,
  email: String,
  password: string,
  age: Number,
  katas: string[]

}
