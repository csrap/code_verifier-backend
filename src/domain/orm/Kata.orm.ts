import { kataEntity} from "../entities/Kata.Entity";

import { LogSuccess, LogError } from "../../utils/Logger"; 
import { IKata } from "../interfaces/IKata.interface";

// CRUD

/**
 * Method to obtain all USers from Collention "User" in Mongo Server
 */

export const getAllKatas = async (page: number, limit: number): Promise <any[] | undefined> => {

try {
  let kataModel = kataEntity(); 
  let response: any = {
  };

  // Search all katas (using pagination)
  await kataModel.find({isDelete: false})
  // .select('name description level starts')
            .limit(limit)
            .skip((page - 1) * limit)
            .exec().then((katas: IKata[]) => {
                response.katas = katas;
            });

     // Count total documents in collection "Kata"
    await kataModel.countDocuments().then((total: number) => {
      response.totalPages = Math.ceil(total / limit);
      response.currentPage = page;
  });
  return response;

} catch (error) {
  LogError(`[ORM ERROR]; Gettind All Katas: ${error}`); 
}
}

// TODO: 
// - Get User By ID
export const getKataByID = async (id:string): Promise<any | undefined> => {
  try {
    let kataModel = kataEntity();

    // Search User By ID
    return await kataModel.findById(id); 

  } catch (error) {
    LogError(`[ORM:ERROR]: Getting Kata By ID: ${error}`); 
  }
}


// - Delete User by ID
export const deleteKatasByID = async (id: string): Promise<any | undefined> => {

  try {
    let kataModel = kataEntity(); 

    //Delete User by id
    return await kataModel.deleteOne({ _id:id})
  } catch (error) {
    LogError(`[ORM:ERROR]: Deleting kata By ID: ${error}`); 
  }
}
// - Create New Kata
export const createKata = async( kata: IKata) : Promise<any | undefined> => {
try {
  let kataModel = kataEntity(); 

  // Create / Insert new Kata
  return await kataModel.create(kata); 

} catch (error) {
  LogError(`[ORM:ERROR]: Creating User: ${error}`); 
}
}

// - Update Kata By ID
export const updateKataByID = async(id: string, kata: IKata) : Promise<any | undefined> => {


  try {
    let kataModel = kataEntity(); 

    //Update User 
    return await kataModel.findByIdAndUpdate(id, kata); 

  } catch (error) {
    LogError(`[ORM:ERROR]: Updating Kata:${id} ${error}`); 
  }

}


// TODO:
// - Get User by Email