import express, { Request, Response} from "express";
import { GoodByeCtrl } from "../controller/ByeControllers"; 
import { LogInfo } from "../utils/Logger";

// Router from express 
let goodByeRouter = express.Router(); 

//  http://localhost:8000/api/hello/?name=Cesar/
goodByeRouter.route('/')
//GET: 
    .get(async (req: Request, res: Response) => {
    //Obtain a Query Param 
    let name: any = req?.query.name;
    LogInfo(`Query Param: ${name}`); 
    // Controller Instance to execute method
    const controller: GoodByeCtrl  = new GoodByeCtrl();
    // Obtain Response 
    const response = await controller.getMessage(name); 
    // Send to the cliente the response
    return res.send(response); 
})

// Export Hello Router
export default goodByeRouter; 