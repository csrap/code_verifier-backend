/**
 * Root Router
 * Redirections to Routers
 */

import express, { Request, Response } from 'express'; 
import helloRouter from './HelloRouter'; 
import { LogInfo } from '../utils/Logger'; 
import goodByeRouter from './GoodByeRouter'; 
import usersRouter from './UserRouter';
import katasRouter from './KatasRouter';
import authRouter from './AuthRouter';

// Server instance
let server = express(); 

// Routes instance
let rootRouter = express.Router(); 

// Activate for request to http: //localhost:8000/api
rootRouter.get('/', (req: Request, res: Response) => {
  LogInfo('GET: http: //localhost:8000/api')
  // Send Hello World
  res.send('Welcome to my API Restful: Express + Nodemon + Jest +TS + Swagger + Mongoose');
});

// Redirection to Routes & Controllers
server.use('/', rootRouter); // http: //localhost:8000/api
server.use('/hello', helloRouter); // http: //localhost:8000/api/hello --> HelloRouter
server.use('/goodbye', goodByeRouter); // http: //localhost:8000/api/hello --> HelloRouter
// Add more routes to the app
// Auth routes
server.use('/auth', authRouter); // http://localhost:8000/api/auth --> AuthRouter

server.use('/users', usersRouter)// http: //localhost:8000/api/users--> UsersRouter
server.use('/katas', katasRouter)// http: //localhost:8000/api/users--> UsersRouter

export default server; 


