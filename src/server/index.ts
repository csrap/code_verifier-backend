import express,  { Express, Request, Response} from "express";

// Swagger
import swaggerUi from 'swagger-ui-express'; 


//  Security
// When the variable it not import install with @types 
import  cors from 'cors'; 
import helmet from 'helmet'; 

//TODO: HTTPS

// Routes
import rootRuter  from '../routes'; 
import mongoose from "mongoose"; 



// Create Express APP
const server: Express = express();
const port: string | number = process.env.PORT || 8000;

// * Swagger Config and roite
server.use(
  '/docs',
  swaggerUi.serve,
  swaggerUi.setup(undefined, {
    swaggerOptions: {
      url: "/swagger.json",
      explorer: true
    }
  })
)

// Define SERVER to use "/api" and iser rootRouter from 'index.ts' routes
// From this point onover: https//localhost:8000/api/...
server.use(
  '/api',
  rootRuter
); 

//* Static server
server.use(express.static('public')); 

// TODO: Mongoose Connection
mongoose.connect('mongodb://localhost:27017/codeverification'); 

// Security Config
server.use(helmet()); 
server.use(cors()); 

// Content Type Config:
server.use(express.urlencoded({
  extended: true,
  limit: '50mb'
})); 
server.use(express.json({
  limit: '50mb'
})); 

// * https://localhost:8000/ -> htpp://locahost:8000/api/

server.get('/', (req: Request, res: Response) => {
  res.redirect('/api'); 
}); 

export default server; 